#!/usr/bin/env python3
from color import *
from models import Player, Theme


class Game:
    def __init__(self):
        self.themes = {}
        self.players = []
        self._players = []
        self.current_player = 0

        self.place = 0
        self.lock_place = False

    def is_playable(self):
        return Player.min_players <= self.player_count <= Player.max_players

    def add_player(self, player_name):
        if self.player_count >= Player.max_players:
            print("They are too many players, the maximume is", Player.max_players)
            return False

        self.players.append(Player(player_name))

        print(player_name, "was added")
        print("They are player number", self.player_count)
        return True

    def add_theme(self, name):
        self.themes[name] = Theme(name)

    def select_theme(self):
        print("What is a Theme ?")

        for idx, name in enumerate(self.themes_name):
            print(f"\t{idx + 1} - Theme {name}")

        choice = int(input())
        self.lock_place = True
        self.place = choice - 1

    @property
    def player_count(self):
        return len(self.players)

    @property
    def theme_count(self):
        return len(self.themes)

    @property
    def players_name(self):
        return [p.name for p in self.players + self._players]

    @property
    def themes_name(self):
        return list(self.themes.keys())

    @property
    def player(self):
        return self.players[self.current_player]

    @property
    def _current_category(self):
        return self.themes_name[self.place % len(self.themes)]

    @property
    def in_penalty_box(self):
        return self.player.in_penalty_box

    def roll(self, roll):
        if not self.is_playable():
            print("Yon can't play", self.player_count)
            return

        print(colored(Color.BOLD, self.player.name, "is the current player"))
        print("They have rolled a", roll)

        if not self.lock_place:
            self.place = self.place + roll
            if self.place > 11:
                self.place = self.place - 12
        self.lock_place = False

        if self.player.in_penalty_box:
            self.player.try_exit()
            if self.player.in_penalty_box:
                print(colored(Color.FAIL, self.player.name, "is not getting out of the penalty box"))
                return
            else:
                print(colored(Color.OKGREEN, self.player.name, "is getting out of the penalty box"))

        # print(self.player.name, "'s new location is", self.place)
        print(colored(Color.OKCYAN, "The category is", self._current_category))
        self._ask_question()

    def _ask_question(self):
        # Pick question in current theme
        print(self.themes[self._current_category].get_questions())

    def correctly_answer(self):
        print(colored(Color.OKGREEN, "Answer was corrent!!!!"))
        self.player.correctly()
        return self.end_party()

    def wrong_answer(self):
        print(colored(Color.FAIL, "Question was incorrectly answered"))
        self.player.wrong()
        return self.end_party()

    def end_party(self):
        nb_win = len([p for p in self.players if p.has_win()])
        return not self.is_playable() or nb_win >= 3 or self.player_count - nb_win <= 1

    def next_player(self):
        self.current_player += 1
        if self.current_player >= self.player_count:
            self.current_player = 0

    def quit(self):
        print(colored(Color.FAIL, self.player.name, "was quit the game !"))
        self._players.append(self.player)
        self.players.remove(self.player)
        return self.end_party()

    def get_podium(self):
        return sorted(self.players + self._players, key=lambda p: p.purse, reverse=True)[:3]


if __name__ == '__main__':
    from random import randrange

    game = Game()

    game.add_theme('Pop')
    game.add_theme('Science')
    game.add_theme('Sports')
    game.add_theme('Rock')

    game.add_player('Chet')
    game.add_player('Pat')
    game.add_player('Sue')

    winner = False
    while True:
        game.roll(randrange(5) + 1)

        if not game.in_penalty_box:
            if randrange(9) == 7:
                winner = game.wrong_answer()
            else:
                winner = game.correctly_answer()

        game.next_player()
        if winner:
            break
