#!/usr/bin/env python3
from random import randrange
from trivia import Game
from models import Player


game = Game()

game.add_player("Player 1")
game.add_player("Player 2")
game.add_player("Player 3")
game.add_player("Player 4")

game.add_theme('Sports')
game.add_theme('Science')
game.add_theme('Pop')
game.add_theme('Techno')

gold = int(input("How many gold is needed to win ? (minimal value : 6)"))
while gold < 6:
    gold = int(input("6 is the minimal gold value. Please, try again."))
Player.win_purse = gold

themes = dict((key, 0) for key in game.themes.keys())
while True:
    game.roll(randrange(5) + 1)
    themes[game._current_category] += 1
    winner = game.correctly_answer()

    print()
    game.next_player()
    if winner:
        print(game.player.name + " has won the game.")
        break

total_theme = sum(i for i in themes.values())
for t, i in themes.items():
    print(f"{t} - {i} times ({i/total_theme*100:.2f}%)")
print(total_theme)
