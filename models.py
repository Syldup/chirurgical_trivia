from color import *
from random import random


class Player:
    win_purse = 6
    min_players = 2
    max_players = 6

    def __init__(self, name):
        self.name = name
        self.purse = 0
        self.cumulative_gain = 1
        self.penalty_count = 0
        self.time_penalty = 0
        self.in_penalty_box = False
        self.jocker = True

    def correctly(self):
        self.purse += self.cumulative_gain
        self.cumulative_gain += 1
        print(colored(Color.WARNING, self.name, "now has", self.purse, "Gold Coins."))

    def wrong(self):
        if not self.in_penalty_box:
            print(colored(Color.FAIL, self.name, "was sent to the penalty box"))
            self.in_penalty_box = True
            self.penalty_count += 1
        self.cumulative_gain = 1

    def use_jocker(self):
        if self.jocker:
            print(colored(Color.OKGREEN, self.name, "has use his jocker !"))
            self.jocker = False

    def try_exit(self):
        p = 1 if self.penalty_count == 0 else 1 / self.penalty_count
        p += 0.1 * self.time_penalty
        if p > 1:
            p = 1
        print(f"{self.name} has been on penalty for {self.time_penalty} round,\n"
              f"he has a {p * 100:.2f}% chance of exiting ...")
        self.time_penalty += 1
        self.in_penalty_box = random() >= p
        if not self.in_penalty_box:
            self.time_penalty = 0

    def has_win(self):
        return self.purse >= self.win_purse


class Theme:
    deck_size = 2

    def __init__(self, name):
        self.name = name
        self.questions = [f"{self.name} Question {i}" for i in range(1, self.deck_size)]
        self.idx = -1

    def get_questions(self):
        self.idx += 1
        if self.idx >= len(self.questions):
            self.idx = -1
        return self.questions[self.idx]
