#!/usr/bin/env python3
from random import randrange
from trivia import Game
from models import Player
from color import *


def get_int(msg=""):
    while True:
        try:
            return int(input(msg))
        except Exception:
            print(colored(Color.FAIL, "Saisi invalide, entrer un nombre !"))


game = Game()

print("Hi!")
nbPlayers = get_int("How many players are there ? ")
while not(Player.min_players <= nbPlayers < Player.max_players):
    if nbPlayers < Player.min_players:
        nbPlayers = get_int("I am sorry, you must be at least 2 players. ")
    elif nbPlayers > Player.max_players:
        nbPlayers = get_int("I am sorry, you can't be more than 6. ")

for i in range(nbPlayers):
    name = input("Name a player : ")
    game.add_player(name)

game.add_theme('Sports')
game.add_theme('Science')
game.add_theme('Pop')

answer = input("Do you want to replace Rock with Techno ? (y/n) ")
if answer.lower() == 'y':
    game.add_theme('Techno')
else:
    game.add_theme('Rock')

Player.win_purse = get_int("How many gold is needed to win ? (minimal value : 6)")
while Player.win_purse < 6:
    Player.win_purse = get_int("6 is the minimal gold value. Please, try again.")

rematch = True
while rematch:
    rematch = False
    end_party = False

    while True:
        game.roll(randrange(5) + 1)

        print("What is your choice ?")
        if not game.in_penalty_box:
            if game.player.jocker:
                print("\t1 - Answer correct")
                print("\t2 - Answer wrong")
                print("\t3 - Use a jocker")
            else:
                print("\t1 - Answer correct")
                print("\t2 - Answer wrong")
        else:
            print("\t1 - Pass")

        print("\t4 - Quit the game")
        choice = 0
        while choice == 0:
            choice = get_int()

            if choice == 4:
                end_party = game.quit()
            elif game.in_penalty_box:
                if choice != 1:
                    choice = 0
            elif choice == 1:
                end_party = game.correctly_answer()
            elif choice == 2:
                end_party = game.wrong_answer()
                game.select_theme()
            elif game.player.jocker and choice == 3:
                end_party = game.player.use_jocker()
            else:
                choice = 0

            if choice == 0:
                print(colored(Color.FAIL, "Veuillez entrer une action valide !"))

        game.next_player()
        if end_party:
            break

    podium = game.get_podium()
    podium_size = len(game.get_podium())

    print("\nThe game is over !")
    print("Here are the champions :")
    print(colored(Color.WARNING, "### 1rst"), "-", podium[0].name, "with", podium[0].purse, "points.")
    print(colored(Color.BOLD, "##  2nd"), " -", podium[1].name, "with", podium[1].purse, "points.")
    if podium_size > 2:
        print(colored(Color.FAIL, "#   3rd"), " -", podium[2].name, "with", podium[2].purse, "points.")

    print("\n--------------------")
    choice = input("Do you wish to rematch ? (y/n) ")
    if choice.lower() == 'y':
        rematch = True
        old_game = game
        print("\n\nNew game :")

        game = Game()
        [game.add_player(p) for p in old_game.players_name]
        [game.add_theme(p) for p in old_game.themes_name]
